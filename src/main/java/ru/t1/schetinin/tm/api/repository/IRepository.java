package ru.t1.schetinin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    interface IRepositoryOptional<M> {

        @NotNull
        Optional<M> findOneById(@Nullable String id);

        @NotNull
        Optional<M> findOneByIndex(@Nullable Integer index);

    }

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {
            @NotNull
            @Override
            public Optional<M> findOneById(@Nullable final String id) {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }
            @NotNull
            @Override
            public Optional<M> findOneByIndex(@Nullable final Integer index) {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }
        };
    }

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @Nullable
    M add(@NotNull M model);

    boolean existsById(@Nullable String id);

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    int getSize();

    @Nullable
    M remove(@Nullable M model);

    @Nullable
    M removeById(@Nullable String id);

    @Nullable
    M removeByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<M> collection);

}